# -*- coding: utf-8 -*-
# @FileName: test_1.py
# @Author: 杨文胜
# @Time: 2022/9/1219:18
# @Software: PyCharm
import jsonpath
import pytest
import requests


class Testcase:
    token =None

    def test_post_login(self):##登录post请求
        # post请求方式代码
        # 接口url
        # 请求参数
        # 请求方式
        # 响应内容
        # 断言结果
        # 打印确认
        url = 'http://39.98.138.157:5000/api/login'
        data = {"password": "123456", "username": "admin"}
        res = requests.post(url=url, json=data)
        Testcase.token = jsonpath.jsonpath(res.json(), '$..token')[0]  # 提取登录成功后的token的值保存在变量中
        print('\n',Testcase.token)
        sjmsg = jsonpath.jsonpath(res.json(), '$..msg')[0]  # 提取登录成功后的msg字段的值的信息
        print('\n',res.json())

        try:
            assert 'success' == sjmsg
            assert 200 == res.status_code
            print('pass333333')
        except:
            print('fail44444')

    def test_get_userinfo(self):#查看get用户信息
        # get请求方式代码
        # 接口url
        # 请求参数
        # 请求方式
        # 响应内容
        # 断言结果
        # 打印确认
        url = 'http://39.98.138.157:5000/api/getuserinfo'
        header1 = {'Token':Testcase.token}
        res1 = requests.get(url=url,headers=header1)
        print('\n',header1)
        sjmsg = jsonpath.jsonpath(res1.json(), '$..nikename')[0]  # 提取登录成功后的msg字段的值的信息
        print('\n',res1.json())
        try:
            assert sjmsg == "疯清扬"
            assert res1.status_code == 200
            print('pass12211212')
        except:
            print('fail22222')


if __name__ == '__main__':
    pytest.main(['-sv', 'test_1.py'])
