# -*- coding: utf-8 -*-
# @FileName: loadfile.py
# @Author: 杨文胜
# @Time: 2022/9/1616:07
# @Software: PyCharm


import openpyxl


def loadfile(self):
    # （读文件、读文件中的表、读表中的内容）
    # 1.找到excel用例文件的位置，读取excel整个文件
    file_path = r".\data\自动化测试用例.xlsx"
    excel_file = openpyxl.load_workbook(file_path)
    # 2.读取excel所有表
    excel_sheet = excel_file.worksheets[0]
    # 3.读取表中的所有内容
    count = excel_sheet.max_row-1
    print('测试用例数量', count)
