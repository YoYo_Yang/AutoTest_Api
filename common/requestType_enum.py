# -*- coding: utf-8 -*-
# @FileName: requestType_enum.py
# @Author: 杨文胜
# @Time: 2022/9/2117:27
# @Software: PyCharm
from enum import Enum


class RequestType(Enum):
    """
       列出有限的，所有可能发送的，请求参数的数据类型，挨个枚举出来
    """
    # json 类型
    JSON = "JSON"
    # PARAMS 类型
    PARAMS = "PARAMS"
    # data 类型
    DATE = "DATE"
    # 文件类型
    FILE = 'FILE'
    # 导出文件
    EXPORT = "EXPORT"
    # 没有请求参数
    NONE = "NONE"

