# -*- coding: utf-8 -*-
# @FileName: pathSetting.py
# @Author: 杨文胜
# @Time: 2022/9/1010:42
# @Software: PyCharm
"""
定义常量，封装(所有路径)
"""
import os


class PathHandle:
    # 路径分隔符，相当于'/'或'\'，（因为windows是\，linux是/，为了运行统一就需要设置一个分隔符）
    SLASH = os.sep

    # 项目根路径的绝对路径
    # ROOT_PATH = os.path.dirname(os.path.abspath(__file__))#返回当前脚本文件的上一层目录的绝对路径，如F:\AutoApiTest\data(logs等其他文件夹)
    ROOT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # 返回当前脚本文件的上两层目录的绝对路径F:\AutoApiTest

    # 项目缓存文件存放路径
    CACHE_PATH = os.path.join(ROOT_PATH + 'cache' + SLASH)
    if not os.path.exists(CACHE_PATH):
        os.mkdir(CACHE_PATH)

    # 项目环境配置存放路径
    CONFIG_PATH = os.path.join(ROOT_PATH + 'config' + SLASH)

    # 项目静态文件存放路径
    FILE_PATH = os.path.join(ROOT_PATH + 'file' + SLASH)

    # 日志路径
    LOG_PATH = os.path.join(ROOT_PATH + 'logs' + SLASH)
    INFO_LOG_PATH = os.path.join(ROOT_PATH, 'logs' + SLASH + 'info.log')
    ERROR_LOG_PATH = os.path.join(ROOT_PATH, 'logs' + SLASH + 'error.log')
    WARNING_LOG_PATH = os.path.join(ROOT_PATH, 'logs' + SLASH + 'warning.log')

    # 测试用例数据路径
    DATA_PATH = os.path.join(ROOT_PATH + 'data' + SLASH)

    # 测试用例处理路径
    CASE_PATH = os.path.join(ROOT_PATH + 'test_case' + SLASH)

    # 测试报告路径
    REPORT_PATH = os.path.join(ROOT_PATH + 'report' + SLASH)

    # 测试报告中的test_case路径
    UTILS_PATH = os.path.join(ROOT_PATH + 'utils' + SLASH)

    # 测试报告，生成的所有测试用例信息，存放路径（test_case）
    report_html_test_case_path = os.path.join(ROOT_PATH,
                                              'report' + SLASH + "html" + SLASH + 'data' + SLASH + "test-cases" + SLASH)

    # 测试报告，生成的附件存放路径（attachments）
    report_html_attachments_path = os.path.join(ROOT_PATH,
                                                'report' + SLASH + "html" + SLASH + 'data' + SLASH + "attachments" + SLASH)
    # 失败测试用例存放路径
    excel_template = os.path.join(ROOT_PATH, 'utils' + SLASH + "allureUtils" + SLASH)


if __name__ == '__main__':
    print(PathHandle.SLASH)
    print(PathHandle.ROOT_PATH)
    print(PathHandle.CACHE_PATH)
    print(PathHandle.CONFIG_PATH)
