# -*- coding: utf-8 -*-
# @FileName: conftest.py
# @Author: 杨文胜
# @Time: 2022/9/1915:13
# @Software: PyCharm

"""
1.学习目标
    掌握conftest.py文件编写格式
2.操作步骤
    2.1 conftest.py文件名不能修改
        conftest.py文件中存放项目所有的fixture，或者全局的fixture，相当于postman的环境变量
        方便对fixture管理和维护
    2.2 在conftest.py定义函数
        在函数前添加@pytest.fixture()装饰器
        在测试用例的函数中传入fixture标识的函数名。
提示：conftest.py文件放在项目的根目录，作用域是全局的。
    conftest.py文件放在某一个包下，作用域只在该包内。
"""
import pytest

#
# @pytest.fixture(scope="session", autouse=Ture)
# def login_token(self):
#     pass
